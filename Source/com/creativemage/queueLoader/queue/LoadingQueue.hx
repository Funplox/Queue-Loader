package com.creativemage.queueLoader.queue ;
import com.creativemage.queueLoader.QueueLoader.DataType;
import com.creativemage.queueLoader.queue.QueueObject;

/**
 * ...
 * @author Alex Kolpakov
 */
@:allow(com.creativemage.queueLoading)
class LoadingQueue
{
	private var queueItemArray:Array<QueueObject>;
	private var currentItem:QueueObject;
	private var uniqueIdCounter:UInt = 0;
	
	public var length(get, null):Int;
	public var itemsLoaded(default, null):Int;
	
	public function new() 
	{
		queueItemArray = new Array<QueueObject>();
	}
	
	public function add(url:String, type:DataType, callback:Dynamic->Void, excludeDuplicated:Bool = false):Int
	{
		// check if a duplicate
		if (excludeDuplicated == true)
		{
			var itemExists:Bool = isRegistered(url);
			
			if (itemExists == true)
				return -1;
		}
		
		var item:QueueObject = new QueueObject();
		item.id = uniqueIdCounter;
		item.url = url;
		item.type = type;
		item.callback = callback;
		
		queueItemArray.push(item);
		
		uniqueIdCounter++;
		
		return item.id;
	}
	
	public function removeItem(itemID:Int):Bool
	{
		for (i in 0...queueItemArray.length)
		{
			var currentItem = queueItemArray[i];
			
			if (currentItem.id == itemID)
			{
				queueItemArray.splice(i, 1);
				itemsLoaded = getItemsLoaded();
				return true;
			}
		}
		
		itemsLoaded = getItemsLoaded();
		return false;
	}
	
	public function removeItemByURL(url:String):Bool
	{
		for (i in 0...queueItemArray.length)
		{
			var currentItem = queueItemArray[i];
			
			if (currentItem.url == url)
			{
				queueItemArray.splice(i, 1);
				itemsLoaded = getItemsLoaded();
				return true;
			}
		}
		
		itemsLoaded = getItemsLoaded();
		return false;
	}
	
	public function removeAll():Void
	{
		itemsLoaded = 0;
		queueItemArray = new Array<QueueObject>();
	}
	
	public function getItem(itemID:Int):QueueObject
	{
		for (i in 0...queueItemArray.length)
		{
			var currentItem = queueItemArray[i];
			
			if (currentItem.id == itemID)
				return currentItem;
		}
		
		return null;
	}
	
	public function getNext():QueueObject
	{	
		itemsLoaded = getItemsLoaded();
		
		for ( i in 0...queueItemArray.length)
		{
			var currentItem:QueueObject = queueItemArray[i];
			if (currentItem.loadComplete == false)
			{
				itemsLoaded = getItemsLoaded();
				return currentItem;
			}
		}
		
		return null;
	}
	
	// PRIVATE METHODS
	
	private function isRegistered(url:String):Bool
	{
		for (i in 0...queueItemArray.length)
		{
			var currentItem = queueItemArray[i];
			
			if (currentItem.url == url)
				return true;
		}
		
		return false;
	}
	
	private function getItemsLoaded():Int
	{
		var loadedItems:Int = 0;
		
		for ( i in 0...queueItemArray.length)
		{
			var currentItem = queueItemArray[i];
			if (currentItem.loadComplete == true)
				loadedItems++;
		}
		
		return loadedItems;
	}
	
	
	// GETTERS AND SETTERS
	
	function get_length():Int 
	{
		return queueItemArray.length;
	}
	
	
}