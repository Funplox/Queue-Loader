package com.creativemage.queueLoader.queue ;
import com.creativemage.queueLoader.QueueLoader;
import com.creativemage.queueLoader.QueueLoader.DataType;
import com.creativemage.queueLoader.queue.LoadingQueue;

/**
 * ...
 * @author Alex Kolpakov
 */
@:allow(com.creativemage.queueLoading)
class QueueObject
{
	public var id:Int;
	public var url:String;
	public var callback:Dynamic->Void;
	public var type:DataType;
	
	public var loadComplete:Bool = false;

	public function new() 
	{
		
	}
	
}