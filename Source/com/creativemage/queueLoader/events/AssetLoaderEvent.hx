package com.creativemage.queueLoader.events ;

import com.creativemage.queueLoader.events.AssetLoaderEvent;
import openfl.display.DisplayObject;
import openfl.events.Event;
/* ...
 * @author Alex Kolpakov
 */
class AssetLoaderEvent extends Event
{
	public static inline var COMPLETE:String = "item load complete";
	public static inline var QUEUE_COMPLETE:String = "queue load complete";
	static public inline var LOADING_ERROR:String = "loading error";
	
	public var loadedData:Dynamic;
	public var loadedImage:DisplayObject;
	
	public var errorMessage:String = "";
	
	
	public function new(type:String, bubbles:Bool=false, cancelable:Bool=false) 
	{
		super(type, bubbles, cancelable);
	}
}