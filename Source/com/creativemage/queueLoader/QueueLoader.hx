package com.creativemage.queueLoader ;

import com.creativemage.queueLoader.events.AssetLoaderEvent;
import com.creativemage.queueLoader.queue.LoadingQueue;
import com.creativemage.queueLoader.queue.QueueObject;
import flash.net.URLRequest;
import haxe.ds.WeakMap;
import openfl.display.Loader;
import openfl.events.ErrorEvent;
import openfl.events.Event;
import openfl.events.EventDispatcher;
import openfl.events.ProgressEvent;
import openfl.net.URLLoader;
import openfl.events.IOErrorEvent;
import openfl.events.HTTPStatusEvent;

/**
 * ...
 * @author Alex Kolpakov
 */

/**
 * Class for loading different type of assets: text, image, sound, animation and binary data.
 * The class is a wrapper of a Loader class with an ability to load queues with handlers for each or a global handler upon queue is finished.
 */

enum DataType
{
	TextData;
	ImageData;
	BinaryData;
}
 
class QueueLoader extends EventDispatcher
{
	private var loader:Loader;
	private var urlLoader:URLLoader;
	private var urlRequest:URLRequest;
	private var queue:LoadingQueue;
	private var currentItem:QueueObject;
	
	private var assetTotalLoadCompleteEvent:AssetLoaderEvent;
	
	private var isLoading:Bool = false;
	
	private var itemProgress:Float = 0;

	public function new() 
	{
		super();
		
		createLoaders();
		poolEventObjects();
		createQueue();
	}
	
	
	// PUBLIC METHODS
	public function load(url:String, ?type:DataType, ?callbackOnComplete:Dynamic->Void, exculdeDuplicates:Bool = false):Int
	{
		if (type == null)
			type = DataType.BinaryData;
		
		var itemID = queue.add(url, type, callbackOnComplete, exculdeDuplicates);
		
		if (isLoading == false)
			moveQueue();
			
		return itemID;
	}
	
	public function abortLoad():Void
	{
		#if flash
		loader.close();
		urlLoader.close();
		#end
		queue.removeAll();
	}
	
	public function abortItemLoad(index:Int):Bool
	{
		#if flash
		if (currentItem.id == index)
			loader.close();
			urlLoader.close();
		#end
		
		queue.removeItem(index);
		moveQueue();
		
		return true;
	}
	
	// PRIVATE METHODS
	
	function createQueue() 
	{
		queue = new LoadingQueue();
	}
	
	function poolEventObjects() 
	{
		assetTotalLoadCompleteEvent = new AssetLoaderEvent(AssetLoaderEvent.QUEUE_COMPLETE);

	}
	
	function createLoaders() 
	{
		loader = new Loader();
		loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoadComplete);
		loader.contentLoaderInfo.addEventListener(IOErrorEvent.NETWORK_ERROR, onLoadError);
		loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onLoadError);
		loader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onProgress);
		
		urlLoader = new URLLoader();
		urlLoader.addEventListener(Event.COMPLETE, onLoadComplete);
		urlLoader.addEventListener(IOErrorEvent.NETWORK_ERROR, onLoadError);
		
		urlRequest = new URLRequest();
	}
	
	function moveQueue() 
	{
		currentItem = queue.getNext();
		
		if (currentItem == null)
		{
			dispatchEvent(assetTotalLoadCompleteEvent);
			return;
		}
		
		isLoading = true;
		loadItem(currentItem);
	}
	
	function loadItem(item:QueueObject):Void
	{
		urlRequest.url = currentItem.url;
		
		switch(item.type)
		{
			case DataType.ImageData:
				{
					loader.load(urlRequest);
				}
			case DataType.BinaryData:
				{
					urlLoader.load(urlRequest);
				}
			case DataType.TextData:
				{
					urlLoader.load(urlRequest);
				}
		}
	}
	
	// EVENT HANDLERS
	
	private function onProgress(e:ProgressEvent):Void 
	{
		itemProgress = e.bytesLoaded / e.bytesTotal;
		
		if (Math.isFinite(itemProgress) == false)
			itemProgress = 1;
	}
	
	private function onLoadComplete(e:Event):Void 
	{		
		isLoading = false;
		currentItem.loadComplete = true;
		
		var assetLoaderEvent:AssetLoaderEvent = new AssetLoaderEvent(AssetLoaderEvent.COMPLETE);
		
		// Set data
		if (currentItem.type == DataType.ImageData)
			assetLoaderEvent.loadedImage = assetLoaderEvent.loadedData = loader.content;
		else
			assetLoaderEvent.loadedData = urlLoader.data;
		
		dispatchEvent(assetLoaderEvent);
		
		// Call the callback method if any
		if (currentItem.callback != null)
			currentItem.callback(assetLoaderEvent.loadedData);
		
		moveQueue();
	}
	
	private function onLoadError(e:IOErrorEvent):Void
	{
		var errorEvent:AssetLoaderEvent = new AssetLoaderEvent(AssetLoaderEvent.LOADING_ERROR);
		errorEvent.errorMessage = e.text;
		dispatchEvent(errorEvent);
		currentItem.loadComplete = true;
		moveQueue();
	}
	
	// GETTERS AND SETTERS
	public function getCurrentItemProgress():Float
	{
		return itemProgress;
	}
	
	public function getGlobalProgress():Float
	{		
		if (queue.itemsLoaded == 0 || queue.length == 0)
			return 0;
		
		return queue.itemsLoaded / queue.length;
	}
	
}